#cd POCO-1.7.8-ALL
#mkdir BUILD && cd BUILD
#cmake -DCMAKE_CXX_FLAGS='-std=c++11 -frtti -fexceptions -fpermissive'  ..
#make -j4

set(CMAKE_BUILD_TYPE Release)
set(BUILD_SHARED_LIBS true)
set(ARCH arch-arm64)
set(ANDROID 1)
set(HOST darwin-x86_64)
set(TOOL aarch64-linux-android)
set(CMAKE_CXX_FLAGS "-frtti -fexceptions -fpic -fpermissive -I${CMAKE_CURRENT_SOURCE_DIR}")
set(CMAKE_CXX_FLAGS_DEBUG -fexceptions)

set(ANDROID_NDK /Users/musictom/Library/Android/sdk/ndk-bundle)
#set( $ANDROID_NDK/toolchains/aarch64-linux-android-4.9/prebuilt/darwin-x86_64/bin)
set(CMAKE_TOOLCHAIN_FILE ${CMAKE_CURRENT_SOURCE_DIR}/cmake/android.toolchain.cmake)
#set(ANDROID_STL stlport_static)
set(ANDROID_NDK /Users/musictom/Library/Android/sdk/ndk-bundle/)
set(ANDROID_TOOLCHAIN_NAME aarch64-linux-android-4.9)
set(TOOLCHAIN ${TOOL}-4.9)

#aarch64-linux-android-4.9       llvm                            mipsel-linux-android-4.9        x86_64-4.9
#arm-linux-androideabi-4.9       mips64el-linux-android-4.9      x86-4.9
set(ANDROID_ABI arm64-v8a)#ANDROID_ABI : "armeabi-v7a" (default), "armeabi", "armeabi-v7a with NEON", "armeabi-v7a with VFPV3", "armeabi-v6 with VFP", "x86", "mips", "arm64-v8a", "x86_64", "mips64"
set(ANDROID_NATIVE_API_LEVEL android-24)

set(CMAKE_C_COMPILER ${ANDROID_NDK}/toolchains/${TOOLCHAIN}/prebuilt/${HOST}/bin/${TOOL}-gcc)
set(CMAKE_CXX_COMPILER ${ANDROID_NDK}/toolchains/${TOOLCHAIN}/prebuilt/${HOST}/bin/${TOOL}-g++)
set(CPACK_PACKAGE_INSTALL_DIRECTORY /Users/musictom/libs/android/${ARCH}/usr/lib)
set(HEADER_DIRECTORY /Users/musictom/libs/android/${ARCH}/usr/include/)

MACRO(INSTALL_HEADERS_WITH_DIRECTORY HEADER_LIST)

    FOREACH(HEADER ${${HEADER_LIST}})
        STRING(REGEX MATCH "(.\\\*)\\\[/\\\]" DIR ${HEADER})
        INSTALL(FILES ${HEADER} DESTINATION ${HEADER_DIRECTORY}/${DIR})
    ENDFOREACH(HEADER)

ENDMACRO(INSTALL_HEADERS_WITH_DIRECTORY)

#add_compile_options(-std=c++11)
#message(STATUS "optional:-std=c++11") 